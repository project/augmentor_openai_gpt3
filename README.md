# OpenAI GPT3 Augmentor

The Augmentor OpenAI GPT3 is a submodule of Augmentor.
It provides an implementation of an Augmentor plugin to allow Augmentor
to interface with OpenAI's REST API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/augmentor_openai_gpt3).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/augmentor_openai_gpt3).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Augmentor](https://www.drupal.org/project/augmentor):


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Configure the user permissions in Administration » People » Permissions:

   - Administer augmentors

   - Users with this permission will see the web services > augmentors
     configuration list page. From here they can add, configure, delete, enable
     and disabled augmentors.

   - Warning: Give to trusted roles only; this permission has security
     implications. Allows full administration access to create and edit
     augmentors.


## Maintainers

Current maintainers:
- Murray Woodman - [murrayw](https://www.drupal.org/u/murrayw)
- Eleo Basili - [eleonel](https://www.drupal.org/u/eleonel)
- Kelvin Wong - [KelvinWong](https://www.drupal.org/u/kelvinwong)
- Naveen Valecha - [naveenvalecha](https://www.drupal.org/u/naveenvalecha)

This project has been sponsored by:
- [Morpht Pty Ltd](https://www.morpht.com/)
  We are a team of dedicated and enthusiastic designers, programmers and site
   builders who know how to get the most from Drupal.
   We work for a variety of clients in government, education, media and
   pharmaceutical sectors.
